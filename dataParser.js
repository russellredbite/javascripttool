 var neo4j = require("neo4j-driver").v1,
 	GraknGraph = require('grakn'), 
	async = require('async'),
	fs = require("fs");

// Database Location
var driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "passw0rd"));

var graph = new GraknGraph('http://localhost:4567', 'grakn');

exports.add = function()
{	
	var session = driver.session(); // Start neo4j connector session
	var count = 0; // Iteration count
	var currentCount = 0; // Node count
	var startDate = new Date().getTime(); // Begin total timer

	// Begin synchronous loop to add nodes and time retrievals
	async.whilst(function() {
  		return count < 5000;
  	},
  	function (next) {
  		// Add 1000 nodes
  		addNodes(count, currentCount, session, function(currentCount, addNodesOut) {

  			// If the database has nodes
			if(currentCount > 0) {
				getRandomNode(currentCount, session, function(randomNodeOut) {
					getChildren(currentCount, session, function(childrenOut) {
						getLeafNodes(currentCount, session, function(leavesOut) {
							getRandomTraversal(currentCount, session, function(traversalOut) {
								// Write times taken to a CSV file for conversion
								var out = (count*1000)+',\t'+addNodesOut+',\t'+randomNodeOut+',\t'+childrenOut+',\t'+leavesOut+',\t'+traversalOut+'\n';
								fs.appendFile('out.txt', out, function (err) {
									// Next iteration
									count++;
									next();
								});
							});
						});
					});
				});
			} else {
				count++;
				next();
			}
		});
	},
  	function(err) {
  		var endDate = new Date();
  		var time = new Date(endDate - startDate);
  		console.log("**********************************");
  		console.log("Time elapsed: "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds());
  		console.log("DONE");
		console.log("**********************************");
  	});
}

function addNodes(count, currentCount, session, callback) {
	console.log(count);
	// Create array of nodes to add
	var nodes = [];
	var currentCount = count*1000;
	for(var i=0;i<1000;i++) {
		nodes.push({count:(currentCount+i)});
	}
	var startAdd = new Date().getTime();
	// Begin neo4j call
	var readTxResultPromise = session.readTransaction(function (transaction) {
		var result = transaction.run('MATCH (r) WHERE NOT (r)-[]->() WITH r LIMIT 1 UNWIND $props AS map CREATE (r)-[:linked_to]->(n) SET n = map', {props:nodes});
		return result;
	});

	// Return neo4j promise
	readTxResultPromise.then(function (result) {
		// Return time taken
		var endAdd = new Date().getTime();
		callback(currentCount, (endAdd-startAdd));
	}).catch(function (error) {
		console.log(error);
		callback(currentCount, false);
	});
}

function getRandomNode(currentCount, session, callback) {
	var randomNode = Math.floor(Math.random() * currentCount);
	var startRandom = new Date().getTime();

	// Begin neo4j call
	var readTxResultPromise = session.readTransaction(function (transaction) {
		var result = transaction.run('match (n{count:{nodeCount}}) return n', {nodeCount:randomNode});
		return result;
	});

	// Return neo4j promise
	readTxResultPromise.then(function (result) {
		// Return time taken
		var endRandom = new Date().getTime();
		callback(endRandom-startRandom);
	}).catch(function (error) {
		console.log(error);
		callback(false);
	});
}

function getChildren(currentCount, session, callback) {
	var randomNodeForChildren = Math.floor(Math.random() * currentCount);
	var startChildren = new Date().getTime();

	// Begin neo4j call
	var readTxResultPromise = session.readTransaction(function (transaction) {
		var result = transaction.run('match (n{count:{nodeCount}}) optional match (n)-[]->(c) return n, count(c)', {nodeCount:randomNodeForChildren});
		return result;
	});

	// Return neo4j promise
	readTxResultPromise.then(function (result) {
		// Return time taken
		var endChildren = new Date().getTime();
		callback(endChildren-startChildren);
	}).catch(function (error) {
		console.log(error);
		callback(false);
	});
}

function getLeafNodes(currentCount, session, callback) {
	var randomNodeForLeaves = Math.floor(Math.random() * currentCount);
	var startLeaves = new Date().getTime();

	// Begin neo4j call
	var readTxResultPromise = session.readTransaction(function (transaction) {
		var result = transaction.run('match (n{count:{nodeCount}}) optional match (n)-[]->(c) where not (c)-[]->() return n, count(c)', {nodeCount:randomNodeForLeaves});
		return result;
	});

	// Return neo4j promise
	readTxResultPromise.then(function (result) {
		// Return time taken
		var endLeaves = new Date().getTime();
		callback(endLeaves-startLeaves);
	}).catch(function (error) {
		console.log(error);
		callback(false);
	});
}

function getRandomTraversal(currentCount, session, callback) {
	var startNodeForTraversal = Math.floor(Math.random() * currentCount);
	var endNodeForTraversal = Math.floor(Math.random() * currentCount);
	var startTraversal = new Date().getTime();

	// Begin neo4j call
	var readTxResultPromise = session.readTransaction(function (transaction) {
		var result = transaction.run('MATCH (s{count:{startNode}}),(e{count:{endNode}}), p = shortestPath((s)-[*..]-(e)) RETURN p', {startNode:startNodeForTraversal, endNode:endNodeForTraversal});
		return result;
	});

	// Return neo4j promise
	readTxResultPromise.then(function (result) {
		// Return time taken
		var endTraversal = new Date().getTime();
		callback(endTraversal-startTraversal);
	}).catch(function (error) {
		console.log(error);
		callback(false);
	});
}