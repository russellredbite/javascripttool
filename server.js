var restify = require('restify');
var dbu = require("./dataParser");

var port    =  '8000';
 
var server = restify.createServer({
    name : "DataParser"
});

server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());
 
server.listen(port, function(){
    console.log('%s listening at %s ', server.name , server.url);
    dbu.add();
});